#!/bin/bash

# shellcheck disable=SC2086

echo -e "\n[INFO] Checking effective user id.";
if [[ $EUID -ne 0 ]]; then
    echo -e "\n[FAIL] Script must be run with sudo or as root; exiting." 1>&2;
    exit 1;
else
    echo -e "\n[INFO] Effective user id is zero (i.e. root or sudo user); proceeding with install.";
fi;

# Script's system package dependencies
DEPS="python2.7 tar coreutils wget binutils libxkbfile1 libatk-bridge2.0 libgtk-3-0 libxss1 openssl unzip"

if [[ "$1" != "-y" ]]; then
    echo -e "\n[INFO] Required dependencies include: $DEPS"
    read -rp "[INFO] Would you like this script to automatically globally install the required dependencies? [Y/n] ";
    if [[ $REPLY == "Y" ]]; then
        echo -e "\n[INFO] Installing dependencies."
	sudo apt-get update && \
        sudo apt-get install -y $DEPS;
    else
        echo -e "\n[WARN] Not installing dependencies; failure to install dependencies manually will result in a failed installation." 1>&2;
    fi;
else
    echo -e "\n[INFO] Installing dependencies."
    sudo apt-get update && \
    sudo apt-get install -y $DEPS;
fi; 

# Default cackey library location
CACKEY_LOC="/usr/lib64/libcackey.so"

# Default VMWare Horizon client coolkey library location
VMWARE_COOLKEY_LOC="/usr/lib/vmware/view/pkcs11/libcoolkeypk11.so"

echo -e "\n[INFO] Testing if cackey already installed.";
if test -f "$CACKEY_LOC"; then
    echo -e "\n[INFO] cackey is already installed; skipping installation.";
else
    echo -e "\n[INFO] cackey is not already installed; installing.";
    sudo apt-get install -y pcsc-tools pcscd && \
    sudo mkdir -p /tmp/cackey/ && \
    (
      cd /tmp/cackey/ && \
      sudo rm -fr /tmp/cackey/* && \
      sudo wget http://cackey.rkeene.org/download/0.7.5/cackey-0.7.5-x86_64-1.tgz && \
      sudo tar -xf cackey* && \
      sudo mkdir -p /usr/lib64/ && \
      sudo cp usr/lib64/libcackey* /usr/lib64/ && \
      rm -fr /tmp/cackey/;
    ) 
    if test -f $CACKEY_LOC; then
        echo -e "\n[INFO] Successfully install cackey.";
    else
        echo -e "\n[FAIL] Failed to install cackey; exiting." 1>&2;
        exit 1;
    fi; 
fi;

echo -e "\n[INFO] Checking if VMWare Horizon client is already installed.";
if hash vmware-view 2>/dev/null; then
    echo -e "\n[INFO] VMWare Horizon client is already installed; skipping installation.";
else
    echo -e "\n[INFO] VMWare Horizon client is not already installed; installing.";
    if ! test -f horizon-5.4.1.bundle; then
    	wget -O horizon-5.4.1.bundle https://download3.vmware.com/software/view/viewclients/CART21FQ1/VMware-Horizon-Client-5.4.1-15988340.x64.bundle
    fi;
    chmod u+x horizon-5.4.1.bundle && \
    sudo TERM=dumb VMWARE_EULAS_AGREED=yes \
    ./horizon-5.4.1.bundle  --console --required \
    --set-setting vmware-horizon-smartcardsmartcardEnable yes \
    --set-setting vmware-horizon-rtavrtavEnable yes \
    --set-setting vmware-horizon-virtual-printing tpEnable yes \
    --set-setting vmware-horizon-tsdrtsdrEnable yes \
    --set-setting vmware-horizon-mmr mmrEnableyes \
    --set-setting vmware-horizon-media-provider mediaproviderEnable yes;
   if hash vmware-view 2>/dev/null; then
       echo -e "\n[INFO] Successfully installed VMware Horizon client.";
   else
       echo -e "\n[FAIL] Failed to install VMWare Horizon client." 1>&2;
       exit 1;
   fi;       
fi;

echo -e "\n[INFO] Replacing VMWare Horizon's default PKI library (coolkey) with cackey.";
{ sudo mkdir -p "/usr/lib/vmware/view/pkcs11/" && \
  declare RANDO_DIR"=$RANDOM" && \
  sudo mkdir -p "/tmp/$RANDO_DIR" && \
  if [[ -e "$VMWARE_COOLKEY_LOC" ]]; then
      echo -e "\n[INFO] Moving VMWare Horizon's default PKI library (coolkey) from current location ($VMWARE_COOLKEY_LOC) to temp dir (/tmp/$RANDO_DIR/coolkey.so)." && \
      sudo mv "$VMWARE_COOLKEY_LOC" "/tmp/$RANDO_DIR/coolkey.so";
  fi;
  echo -e "\n[INFO] Symlinking cackey into default coolkey location ($VMWARE_COOLKEY_LOC)." && \
  sudo ln -s "$CACKEY_LOC" "$VMWARE_COOLKEY_LOC" && \
  sudo ls -hal "$VMWARE_COOLKEY_LOC" && \
  echo -e "\n[INFO] Successfully replaced coolkey with cackey.";
} || \
{ echo -e "\n[FAIL] Failed to successfully replace coolkey with cackey; exiting."; \
  exit 1; }

{ echo -e "\n[INFO] Determining if DoD certificate installation is required." && \
  wget -S --spider --timeout 10 https://afrcdesktops.us.af.mil && \
  echo -e "\n[INFO] Desktop Anywhere login gateway is accessible; DoD certificate installation is not required." && \
  cert_install_required=false; } || \
{ echo -e "\n[INFO] Desktop Anywhere login gateway is not accessible; DoD certificate installation is required.";
  cert_install_required=true; };
if ${cert_install_required}; then
    cert_dir="/usr/local/share/ca-certificates/dod/";
    { mkdir -p .certs/dod/ || { echo -e "\n[FAIL] Failed to make certs sub-directory tree; exiting."; exit 1; } } && \
    { echo -e "\n[INFO] Fetching latest DoD CA Bundles." && \
      wget -O .certs/dod.zip https://dl.dod.cyber.mil/wp-content/uploads/pki-pke/zip/certificates_pkcs7_v5-6_dod.zip && \
      echo -e "\n[INFO] Successfully fetched latest DoD CA Bundle."; } || \
      { echo -e "\n[FAIL] Failed to fetch latest DoD CA Bundle; exiting." 2>&1; \
        exit 1; } && \
    { echo -e "\n[INFO] Extracting DoD certificates into a temp sub-directory."
      unzip -o .certs/dod.zip -d .certs/dod/; } || \
      { echo -e "\n[FAIL] Failed to extract DoD certificates into certs sub-directory; exiting." 2>&1; \
        exit 1; } && \
    { cd .certs/dod/* || { echo -e "\n[FAIL] Failed to cd into DoD certs sub-directory; exiting."; exit 1; } } && \
    { echo -e "\n[INFO] Verifying DoD certificate checksums." && \
      openssl smime -verify -in ./*.sha256 -inform DER -CAfile ./*.pem | \
      while IFS= read -r line; do
	  echo "${line%$'\r'}";
      done | \
      sha256sum -c; } || \
      { echo -e "\n[FAIL] File checksums do not match those listed in the checksum file; exiting." 2>&1; \
        exit 1; } && \
    { mkdir -p ${cert_dir} || \
    { echo -e "\n[FAIL] Failed to make dod sub-directory (${cert_dir}); exiting." 1>&2; exit 1; } };
    echo -e "\n[INFO] Converting DoD certificates to plaintext format and staging for inclusion in system CA trust.";
    for p7b_file in *.pem.p7b; do
        pem_file="${p7b_file//.p7b/}"
        { echo -e "\n[INFO] Converting ${p7b_file} to ${pem_file}" && \
          openssl \
              pkcs7 \
                  -in "${p7b_file}" \
                  -print_certs \
                  -out "${pem_file}";} || \
        { echo -e "\n[FAIL] Failed to convert ${p7b_file} to ${pem_file}; exiting." 1>&2; \
          exit 1; } && \
	echo -e "\n[INFO] Splitting CA bundle file (${pem_file}) into individual cert files and staging for inclusion in system CA trust." && \
 	while read -r line; do
 	   if [[ "${line}" =~ END.*CERTIFICATE ]]; then
 	       cert_lines+=( "${line}" );
	       : > "${cert_dir}${individual_certs[ -1]}.crt";
 	       for cert_line in "${cert_lines[@]}"; do
 	           echo "${cert_line}" >> "${cert_dir}${individual_certs[ -1]}.crt";
               done;
 	       cert_lines=( );
 	   elif [[ "${line}" =~ ^[[:space:]]*subject=.* ]]; then
	       individual_certs+=( "${BASH_REMATCH[0]//*CN = /}" );
 	       cert_lines+=( "${line}" );
 	   elif [[ "${line}" =~ ^[[:space:]]*$ ]]; then
               :;
 	   else
 	       cert_lines+=( "${line}" );
 	   fi;
 	done < "${pem_file}";
    done;
    for p7b_file in *.der.p7b; do
        der_file="${p7b_file//.p7b/}"
        { echo -e "\n[INFO] Converting ${p7b_file} to ${der_file}" && \
          openssl \
              pkcs7 \
              -in "${p7b_file}" \
              -inform DER \
              -print_certs \
              -out "${der_file}"; } || \
        { echo -e "\n[FAIL] Failed to convert ${p7b_file} to ${der_file}; exiting." 1>&2; \
          exit 1; };
	echo -e "\n[INFO] Splitting CA bundle file (${der_file}) into individual cert files and staging for inclusion in system CA trust." && \
 	while read -r line; do
 	   if [[ "${line}" =~ END.*CERTIFICATE ]]; then
 	       cert_lines+=( "${line}" );
	        : > "${cert_dir}${individual_certs[ -1]}.crt"
 	       for cert_line in "${cert_lines[@]}"; do
 	           echo "${cert_line}" >> "${cert_dir}${individual_certs[ -1]}.crt";
               done;
 	       cert_lines=( );
 	   elif [[ "${line}" =~ ^[[:space:]]*subject=.* ]]; then
	           individual_certs+=( "${BASH_REMATCH[0]//*CN = /}" );
 	           cert_lines+=( "${line}" );
 	   elif [[ "${line}" =~ ^[[:space:]]*$ ]]; then
               :;
 	   else
 	       cert_lines+=( "${line}" );
 	   fi;
 	done < "${der_file}";
    done && \
    { cd - &>/dev/null || exit 1; } && \
    echo -e "\n[INFO] Found a total of ${#individual_certs[@]} individual certs inside of CA bundles." && \
    # Placing all individual_certs into a key in uniq_cert array to deduplicate non-unique certs
    # This assumes that CN values for all certs are sufficiently unique keys to act as UIDs
    declare -A uniq_certs && \
    for individual_cert in "${individual_certs[@]}"; do
        uniq_certs["$individual_cert"]="${individual_cert}";
    done && \
    echo -e "\n[INFO] Found a total of ${#uniq_certs[@]} unique certs inside of CA bundles." && \
    { echo -e "\n[INFO] The following DoD certificate files are staged for inclusion in the system CA trust:" && \
      total_staged=0 && \
      for staged_file in ${cert_dir}*; do
          echo "${staged_file}";
	  total_staged="$((total_staged+1))";
      done; } && \
      echo "===END OF LIST===" && \
    # This ensures the user is aware if any certificates appear to have been left out entirely by accident
    # While a check is still performed at the end that Desktop Anywhere is accessible, this ensures other sites are too
    { if [[ "${total_staged}" != "${#uniq_certs[@]}" ]]; then
          echo -e "\n[FAIL] Failed to stage all previously discovered unique certificates." 1>&2;
	  exit 1;
      fi; };
    { echo -e "\n[INFO] Adding staged DoD certificates (and any other previously staged certs) to system CA trust." && \
      update-ca-certificates --verbose --fresh && \
      echo -e "\n[INFO] Successfully added staged certificates to system CA trust."; } || \
    { echo -e "\n[FAIL] Failed to add staged certificates to system CA trust; exiting."; \
      exit 1;};
    { echo -e "\n[INFO] Verifying that Desktop Anywhere login gateway is accessible after certificate installation." && \
      wget -S --spider --timeout 10 https://afrcdesktops.us.af.mil && \
      echo -e "\n[INFO] Desktop Anywhere login gateway is accessible after certificate installation."; } || \
    { echo -e "\n[FAIL] Desktop Anywhere login gateway is not accessible after certificate installation; exiting." 2>&1;
      exit 1; };
    { rm -fr .certs/ || echo -e "[WARN] Failed to clean up temporary .certs directory."; };
fi;

echo -e "\n[INFO] Adding TLS 1.1 support to VMWare Horizon client configuration.";
{ sudo mkdir -p "$HOME/.vmware/" && \
  sudo chown -R "${SUDO_USER}":"${SUDO_USER}" "$HOME/.vmware/" && \
  echo "view.sslProtocolString = \"TLSv1.1\"" >> "$HOME/.vmware/view-preferences" && \
  sudo chown -R "${SUDO_USER}":"${SUDO_USER}" "$HOME/.vmware/view-preferences" && \
  echo -e "\n[INFO] Successfully added TLS 1.1 support to VMWare Horizon configuration."; } || \
{ echo -e "\n[FAIL] Failed to add TLS 1.1 support to VMWare Horizon configuration; exiting."; \
  exit 1; };

echo -e "\nInstallation complete! Launch Horizon and add the following server:";
echo -e "\nVDI Address: https://afrcdesktops.us.af.mil";

